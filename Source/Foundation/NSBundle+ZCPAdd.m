//
//  NSBundle+ZCPAdd.m
//  ZCPCategory
//
//  Created by zcp on 2019/5/22.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "NSBundle+ZCPAdd.h"

@implementation NSBundle (ZCPAdd)

/// 应用名称
+ (NSString *)appDisplayName {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleDisplayName"];
}
/// 应用包名
+ (NSString *)appBundleIdentifier {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"];
}
/// 应用版本号
+ (NSString *)appVersion {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
}
/// 编译版本号
+ (NSString *)appBuildVersion {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
}

@end
