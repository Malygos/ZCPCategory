//
//  NSString+Category.h
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDateFormatter+Category.h"
#import "NSDate+Category.h"

@interface NSString (Category)

#pragma mark - 
/**
 判断是否包含特定字符串
 */
- (BOOL)contains:(NSString *)str;

/**
 json字符串转换为json对象
 */
- (id)JSONObject;

/**
 NSNumber保持精度转换成字符串
 */
+ (NSString *)formatWithDecimalNumber:(NSNumber *)number;

/**
 链式快速拼接字符串
 */
- (NSString *(^)(NSString *appendStr))appendString;

+ (NSString *)UUIDString;

#pragma mark - iconfont

/**
 将hex字符串转换成iconfont字符串

 @param hexString hex字符串，如"f846"、"e507"
 @return iconfont字符串，如"\U0000f846"、"\U0000e507"
 */
+ (NSString *)iconFromHexString:(NSString *)hexString;

#pragma mark - 日期/字符串转换

/**
 日期转换成字符串 yyyy-MM-dd格式
 */
+ (NSString *)stringFromDate:(NSDate *)date;

/**
 日期转换成字符串 yyyy-MM-dd HH-mm-ss格式
 */
+ (NSString *)stringFromYDMHmsDate:(NSDate *)date;

/**
 日期转换成字符串 自定义转换格式
 */
+ (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)format;

/**
 转换成日期 yyyy-MM-dd格式
 */
- (NSDate *)toDate;

/**
 转换成日期 yyyy-MM-dd HH:mm:ss格式
 */
- (NSDate *)toYDMHmsDate;

/**
 转换成日期 自定义格式
 */
- (NSDate *)toDateWithDateFormat:(NSString *)format;


#pragma mark - Remove Emoji
/**
 判断字符串中是否含有Emoji表情
 */
- (BOOL)isIncludeEmoji;

/**
 移除字符串中的Emoji表情。不改变原字符串，返回移除后的字符串
 */
- (NSString *)stringRemoveEmoji;

#pragma mark - Chinese

/**
 判断是否为汉字
 */
+ (BOOL)isChinesecharacter:(NSString *)string;

/**
 计算汉字的个数
 */
+ (NSInteger)chineseCountOfString:(NSString *)string;

/**
 计算字母的个数
 */
+ (NSInteger)characterCountOfString:(NSString *)string;

#pragma mark - URL

/**
 判断是否是web url
 */
- (BOOL)isWebURL;

/**
 获取url的参数
 */
- (NSDictionary *)getURLParams;

/**
 检测字符串中的url，使用系统方法，根据://检测
 
 @return NSArray urlString数组
 */
- (NSArray <NSString *>*)detectLink;

/**
 url编码
 */
- (NSString *)urlEncodeString;

/**
 url解码
 */
- (NSString *)urlDecodeString;

@end
