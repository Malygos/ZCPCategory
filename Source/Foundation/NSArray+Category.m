//
//  NSArray+Category.m
//  ZCPKit
//
//  Created by zhuchaopeng on 16/9/18.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "NSArray+Category.h"

@implementation NSArray (Category)

/// 安全获取索引下的元素
- (id)safeObjectAtIndex:(NSUInteger)index {
    id obj = nil;
    
    if (self.count >= index) {
        obj = [self objectAtIndex:index];
    }
    return obj;
}

@end

@implementation NSMutableArray (Category)

/// 安全添加元素
- (void)safeAddObject:(id)anobject {
    if(anobject && ![anobject isKindOfClass:[NSNull class]]) {
        [self addObject:anobject];
    }
}

@end
