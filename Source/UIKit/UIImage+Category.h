//
//  UIImage+Category.h
//  ZCPCategory
//
//  Created by zcp on 2019/5/16.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Category)

#pragma mark - color
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color radiu:(CGFloat)radiu;

#pragma mark - screenshot
+ (UIImage *)screenshotImage;
+ (UIImage *)screenshotImageWith:(CGRect)rect;

#pragma mark - scale
+ (UIImage *)image:(UIImage *)image scaleToSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
