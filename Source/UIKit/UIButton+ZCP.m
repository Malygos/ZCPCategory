//
//  UIButton+ZCP.m
//  ZCPCategory
//
//  Created by zcp on 2019/5/17.
//

#import "UIButton+ZCP.h"
#import <objc/runtime.h>

@interface UIButton ()

@property (nonatomic, copy) void(^zcpButtonActionBlock)(UIButton * _Nonnull button);

@end

@implementation UIButton (ZCP)

#pragma mark - Action

- (void)addAction:(void (^)(UIButton * _Nonnull))block {
    self.zcpButtonActionBlock = block;
    [self addTarget:self action:@selector(action:event:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addAction:(void (^)(UIButton * _Nonnull))block forControlEvents:(UIControlEvents)controlEvents {
    self.zcpButtonActionBlock = block;
    [self addTarget:self action:@selector(action:event:) forControlEvents:controlEvents];
}

- (void)addActionNoMultiTouchFilter:(void (^)(UIButton * _Nonnull))block {
    self.zcpButtonActionBlock = block;
    [self addTarget:self action:@selector(noMultiTouchFilterAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)addActionNoMultiTouchFilter:(void (^)(UIButton * _Nonnull))block forControlEvents:(UIControlEvents)controlEvents {
    self.zcpButtonActionBlock = block;
    [self addTarget:self action:@selector(noMultiTouchFilterAction:) forControlEvents:controlEvents];
}

#pragma mark <help method>

- (void)action:(id)sender event:(UIEvent *)event {
    if ([event.allTouches anyObject].tapCount == 1) {
        if (self.zcpButtonActionBlock) {
            self.zcpButtonActionBlock(self);
        }
    }
}

- (void)noMultiTouchFilterAction:(id)sender {
    if (self.zcpButtonActionBlock) {
        self.zcpButtonActionBlock(self);
    }
}

- (void (^)(UIButton * _Nonnull))zcpButtonActionBlock {
    return objc_getAssociatedObject(self, @selector(zcpButtonActionBlock));
}

- (void)setZcpButtonActionBlock:(void (^)(UIButton * _Nonnull))zcpButtonActionBlock {
    objc_setAssociatedObject(self, @selector(zcpButtonActionBlock), zcpButtonActionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - EdgeInsets

- (void)updateSubviewFrameWithTitleSize:(CGSize)titleSize imageSize:(CGSize)imageSize position:(NSInteger)position gap:(CGFloat)gap {
    
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    CGFloat titleWidth = titleSize.width;
    CGFloat titleHeight = titleSize.height;
    CGFloat halfImageWidth = imageWidth / 2;
    CGFloat halfImageHeight = imageHeight / 2;
    CGFloat halfTitleWidth = titleWidth / 2;
    CGFloat halfTitleHeight = titleHeight / 2;
    CGFloat halfGap = gap / 2;
    
    switch (position) {
        case 0:// 左图右字
        {
            self.titleEdgeInsets = UIEdgeInsetsMake(0, halfGap, 0, -halfGap);
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -halfGap, 0, halfGap);
        }
            break;
        case 1:// 右图左字
        {
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth - halfGap, 0, imageWidth + halfGap);
            self.imageEdgeInsets = UIEdgeInsetsMake(0, titleWidth + halfGap, 0, -titleWidth - halfGap);
        }
            break;
        case 2:// 上图下字
        {
            self.titleEdgeInsets = UIEdgeInsetsMake(halfImageHeight + halfGap, -halfImageWidth, -(halfImageHeight + halfGap), halfImageWidth);
            self.imageEdgeInsets = UIEdgeInsetsMake(-(halfTitleHeight + halfGap), halfTitleWidth, halfTitleHeight + halfGap, -halfTitleWidth);
        }
            break;
        case 3:// 下图上字
        {
            self.titleEdgeInsets = UIEdgeInsetsMake(-(halfImageHeight + halfGap), -halfImageWidth, (halfImageHeight + halfGap), halfImageWidth);
            self.imageEdgeInsets = UIEdgeInsetsMake(halfTitleHeight + halfGap, halfTitleWidth, -(halfTitleHeight + halfGap), -halfTitleWidth);
        }
            break;
        default:
            break;
    }
}

@end
