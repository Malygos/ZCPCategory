//
//  UIDevice+ZCPAdd.m
//  ZCPCategory
//
//  Created by zcp on 2019/5/22.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "UIDevice+ZCPAdd.h"
#import <sys/utsname.h>
#import <AdSupport/AdSupport.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>
#import <Photos/Photos.h>
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>
#import <EventKit/EventKit.h>

/**
 1.运营商信息
 
 国际移动用户识别码（IMSI） international mobile subscriber identity 国际上为唯一识别一个移动用户所分配的号码。
 IMSI：MCC+MNC+MSIN
 MCC：Mobile Country Code，移动国家码，MCC的资源由国际电联（ITU）统一分配和管理，唯一识别移动用户所属的国家，共3位，中国为460
 MNC：Mobile Network Code，移动网络码，共2位，比如中国移动TD系统使用00，中国联通GSM系统使用01，中国移动GSM系统使用02，中国电信CDMA系统使用03等
 MSIN：Mobile Subscriber Identification Number共有10位，其结构如下：09+M0M1M2M3+ABCD
 
 MCC用来区别出每个用户的来自的国家，因此可以实现国际漫游。在同一个 国家内，如果有多个移动网络运营商，可以通过MNC来进行区别。一个典型的IMSI号码为460030912121001。
 
 2.应用唯一标识
 移动广告商和游戏网络运营商往往需要通过UDID用来识别玩家用户，并对用户活动进行跟踪。
 
 UDID：Unique Device Identifier。Apple公司于2013年5月1日开始，拒绝采集UDID的App上架App Store。
 OpenUDID：https://github.com/ylechelle/OpenUDID/blob/master/OpenUDID.m
 MAC Address：iOS7以后不可用，获取到的值固定为02:00:00:00:00:00
 IDFA：广告标示符是由系统存储，但是有几种情况会重新生成。如果用户完全重置系统（(设置程序 -> 通用 -> 还原 -> 还原位置与隐私) ，这个广告标示符会重新生成。另外如果用户明确的还原广告(设置程序-> 通用 -> 关于本机 -> 广告 -> 还原广告标示符) ，那么广告标示符也会重新生成。此外Appstore禁止不使用广告而采集IDFA的app上架
 IDFV：应用提供商标示符。同一个应用提供商开发的app共用同一个IDFV。但是如果将该供应商的所有App卸载，重装后IDFV会重新生成。
 UUID：Universally Unique Identifier，通用唯一识别码。CFUUID、NSUUID只提供了生成方法，使用它每次获得到的值不同，具体如何存储需要自己处理。
 */

@implementation UIDevice (ZCPAdd)

// ----------------------------------------------------------------------
#pragma mark - Size
// ----------------------------------------------------------------------

+ (CGFloat)stautsBarHeight {
    if ([UIDevice isIPhoneXSeries]) {
        return 44;
    }
    return 20;
}

+ (CGFloat)stautsBarAndNaviBarHeight {
    if ([UIDevice isIPhoneXSeries]) {
        return 88;
    }
    return 64;
}

+ (CGFloat)tabBarHeight {
    if ([UIDevice isIPhoneXSeries]) {
        return 83;
    }
    return 49;
}

+ (CGFloat)safeAreaInsetsBottomHeight {
    if ([UIDevice isIPhoneXSeries]) {
        return 34;
    }
    return 0;
}

// ----------------------------------------------------------------------
#pragma mark - Device Information
// ----------------------------------------------------------------------

/// 设备名称，如：zcp的iphone
+ (NSString *)deviceName {
    return [UIDevice currentDevice].name;
}
/// 系统名称，如：iOS
+ (NSString *)OSName {
    return [UIDevice currentDevice].systemName;
}
/// 系统版本，如：12.0
+ (NSString *)OSVersion {
    return [UIDevice currentDevice].systemVersion;
}
/// 运营商
+ (ZCPChinaCarrierType)carrier {
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = info.subscriberCellularProvider;
    NSString *mcc = [carrier.mobileCountryCode substringWithRange:NSMakeRange(0, 3)];
    NSInteger mnc = [[carrier.mobileNetworkCode substringWithRange:NSMakeRange(0, 2)] integerValue];
    
    if ([mcc isEqualToString:@"460"]) {
        switch (mnc) {
            case 00:
            case 02:
            case 07:
                return ZCPChinaCarrierTypeChinaMobile;
            case 01:
            case 06:
                return ZCPChinaCarrierTypeChinaUnicom;
            case 03:
            case 05:
                return ZCPChinaCarrierTypeChinaTelecom;
            case 20:
                return ZCPChinaCarrierTypeChinaTietong;
            default:
                break;
        }
    }
    return ZCPChinaCarrierTypeUnknown;
}
/// 国家
+ (NSString *)countryCode {
    return [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
}
/// 语言
+ (NSString *)language {
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSArray *languages          = [defaults objectForKey:@"AppleLanguages"];
    NSString *language          = [languages objectAtIndex:0];
    return language;
}
/// 时区
+ (NSString *)timeZone {
    return [NSTimeZone systemTimeZone].name;
}
/// 分辨率
+ (NSString *)resolution {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    return [NSString stringWithFormat:@"%.fx%.f", width, height];
}
/// 设备型号，如：iPhone11,6
+ (NSString *)deviceModel {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}
/// 设备型号名称，如：iPhone XS MAX
+ (NSString *)deviceModelName {
    NSString *deviceModel = [self deviceModel];
    
    if ([deviceModel isEqualToString:@"iPhone1,1"]) return @"iPhone 1G";
    if ([deviceModel isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    if ([deviceModel isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    if ([deviceModel isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    if ([deviceModel isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    if ([deviceModel isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    if ([deviceModel isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    if ([deviceModel isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    if ([deviceModel isEqualToString:@"iPhone5,3"]) return @"iPhone 5C";
    if ([deviceModel isEqualToString:@"iPhone5,4"]) return @"iPhone 5C";
    if ([deviceModel isEqualToString:@"iPhone6,1"]) return @"iPhone 5S";
    if ([deviceModel isEqualToString:@"iPhone6,2"]) return @"iPhone 5S";
    if ([deviceModel isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    if ([deviceModel isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    if ([deviceModel isEqualToString:@"iPhone8,1"]) return @"iPhone 6S";
    if ([deviceModel isEqualToString:@"iPhone8,2"]) return @"iPhone 6S Plus";
    if ([deviceModel isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    if ([deviceModel isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    if ([deviceModel isEqualToString:@"iPhone9,3"]) return @"iPhone 7";
    if ([deviceModel isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    if ([deviceModel isEqualToString:@"iPhone9,4"]) return @"iPhone 7 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,1"]) return @"iPhone 8";
    if ([deviceModel isEqualToString:@"iPhone10,4"]) return @"iPhone 8";
    if ([deviceModel isEqualToString:@"iPhone10,2"]) return @"iPhone 8 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,5"]) return @"iPhone 8 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,3"]) return @"iPhone X";
    if ([deviceModel isEqualToString:@"iPhone10,6"]) return @"iPhone X";
    if ([deviceModel isEqualToString:@"iPhone11,8"]) return @"iPhone XR";
    if ([deviceModel isEqualToString:@"iPhone11,2"]) return @"iPhone XS";
    if ([deviceModel isEqualToString:@"iPhone11,4"]) return @"iPhone XS MAX";
    if ([deviceModel isEqualToString:@"iPhone11,6"]) return @"iPhone XS MAX";
    
    return @"";
}

/// 是否是iPhone
+ (BOOL)isIPhone {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}
/// 是否是iPhone Plus
+ (BOOL)isIphonePlus {
    return ([self isIPhone] && [[UIScreen mainScreen] scale] == 3.0);
}
/// 是否是iPad
+ (BOOL)isIpad {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
/// 是否是iPhoneX系列
+ (BOOL)isIPhoneXSeries {
    BOOL iPhoneXSeries = NO;
    if (![self isIPhone]) {
        return iPhoneXSeries;
    }
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            iPhoneXSeries = YES;
        }
    }
    return iPhoneXSeries;
}

// ----------------------------------------------------------------------
#pragma mark - Identifier
// ----------------------------------------------------------------------

/// UDID
+ (NSString *)UDID {
    return @"";
}
/// 设备mac地址
+ (NSString *)macAddress {
    return @"02:00:00:00:00:00";
}
/// 广告标示符
+ (NSString *)IDFA {
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}
/// 应用提供商标示符
+ (NSString *)IDFV {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

// ----------------------------------------------------------------------
#pragma mark - Security
// ----------------------------------------------------------------------

/// 判断App是否被破解
+ (BOOL)isPirated {
    
    /*
     通过对比正版ipa文件与破解的ipa文件，发现破解后的主要区别有两点：
     1.SC_Info目录被移除，该目录包含两个文件：
     (1)appname.sinf为metadata文件
     (2)appname.supp为解密可执行文件的密钥
     2.iTunesMetadata.plist文件被移除，该文件用来记录app的基本信息，例如购买者的appleID，app购买时间、app支持的设备体系结构，app的版本、app标识符
     */
    BOOL isPirated                  = NO;
    
    NSString * bundlePath           = [[NSBundle mainBundle] bundlePath];
    
    // SC_Info
    if (![[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/SC_Info",bundlePath]]) {
        isPirated                   = YES;
    }
    // iTunesMetadata.​plist
    if (![[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/iTunesMetadata.​plist",bundlePath]]) {
        isPirated                   = YES;
    }
    
    return isPirated;
}

/// 判断设备是否越狱
+ (BOOL)isJailbroken {
    BOOL isJailbroken = NO;
    
    NSString *cydiaPath = @"/Applications/Cydia.app";
    NSString *aptPath = @"/private/var/lib/apt/";//Cydia各个源下载的package，里面是补丁和Cydia市场中的软件信息
    NSString *dylibPath = @"/Library/MobileSubstrate/MobileSubstrate.dylib";//越狱后挂载的动态🔗库
    NSString *etcApt = @"/etc/apt";//Cydia添加的源地址和信任的配置文件
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:cydiaPath]) {
        isJailbroken        = YES;
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:aptPath]) {
        isJailbroken        = YES;
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:dylibPath]) {
        isJailbroken        = YES;
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:etcApt]) {
        isJailbroken        = YES;
    }
    return isJailbroken;
}

// ----------------------------------------------------------------------
#pragma mark - Permission Available
// ----------------------------------------------------------------------

/**
 // Privacy - Location Always Usage Description
 // Privacy - Location Usage Description
 
 // Privacy - Location When In Use Usage Description
 //    [locationManager requestWhenInUseAuthorization];
 // Privacy - Location Always and When In Use Usage Description
 [locationManager requestAlwaysAuthorization];
 
 CLLocationManagerDelegate
 - (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
 
 */
+ (ZCPPermissionAvailableStatus)isLocationAvailable {
    if (![CLLocationManager locationServicesEnabled]) {
        return ZCPPermissionAvailableStatusNotAvailable;
    }
    
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    CLAuthorizationStatus authStatus = [CLLocationManager authorizationStatus];
    
    switch (authStatus) {
        case kCLAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusNotAvailable;
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
    return status;
}

+ (BOOL)isPushAvailable {
    if (@available(iOS 8.0, *)) {
        if ([[UIApplication sharedApplication] currentUserNotificationSettings].types  == UIUserNotificationTypeNone) {
            return NO;
        }
    } else {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
        if ([[UIApplication sharedApplication] enabledRemoteNotificationTypes]  == UIRemoteNotificationTypeNone) {
            return NO;
        }
        #pragma clang diagnostic pop
    }
    return YES;
}

+ (ZCPPermissionAvailableStatus)isCameraAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    switch (authStatus) {
        case AVAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case AVAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        default:
            break;
    }
    return status;
}

+ (ZCPPermissionAvailableStatus)isAudioAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    
    switch (authStatus) {
        case AVAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case AVAuthorizationStatusRestricted:
        case AVAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusNotAvailable;
            break;
        case AVAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
    return status;
}

+ (ZCPPermissionAvailableStatus)isPhotoAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0 //iOS 8.0以下使用AssetsLibrary.framework
    ALAuthorizationStatus authStatus = [ALAssetsLibrary authorizationStatus];
    switch (authStatus) {
        case ALAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case ALAuthorizationStatusRestricted:
        case ALAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusNotAvailable;
            break;
        case ALAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
#else   //iOS 8.0以上使用Photos.framework
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    switch (authStatus) {
        case PHAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case PHAuthorizationStatusRestricted:
        case PHAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        case PHAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
#endif
    return status;
}

+ (ZCPPermissionAvailableStatus)isAddressAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_9_0
    ABAuthorizationStatus authStatus = ABAddressBookGetAuthorizationStatus();
    switch (authStatus) {
        case kABAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case kABAuthorizationStatusRestricted:
        case kABAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        case kABAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
#else
    CNAuthorizationStatus authStatus = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    switch (authStatus) {
        case CNAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case CNAuthorizationStatusRestricted:
        case CNAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        case CNAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
    }
#endif
    return status;
}

+ (ZCPPermissionAvailableStatus)isCalendarAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    EKAuthorizationStatus authStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (authStatus) {
        case EKAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case EKAuthorizationStatusRestricted:
        case EKAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        case EKAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
    return status;
}

+ (ZCPPermissionAvailableStatus)isRemindAvailable {
    ZCPPermissionAvailableStatus status = ZCPPermissionAvailableStatusNotDetermined;
    EKAuthorizationStatus authStatus = [EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder];
    
    switch (authStatus) {
        case EKAuthorizationStatusNotDetermined:
            status = ZCPPermissionAvailableStatusNotDetermined;
            break;
        case EKAuthorizationStatusRestricted:
        case EKAuthorizationStatusDenied:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        case EKAuthorizationStatusAuthorized:
            status = ZCPPermissionAvailableStatusAvailable;
            break;
        default:
            break;
    }
    return status;
}

+ (BOOL)isQRCodeScanAvailable {
    // Device
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Input
    NSError* error = nil;
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    // Output
    AVCaptureMetadataOutput* output = [[AVCaptureMetadataOutput alloc] init];
    // Session
    AVCaptureSession* session = [[AVCaptureSession alloc] init];
    [session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([session canAddInput:input]) {
        [session addInput:input];
    }
    
    if ([session canAddOutput:output]) {
        [session addOutput:output];
    }
    
    NSArray* array = output.availableMetadataObjectTypes;
    for (NSString* objectType in array) {
        if ([objectType isEqualToString:AVMetadataObjectTypeQRCode]) {
            return YES;
        }
    }
    return NO;
}

@end
