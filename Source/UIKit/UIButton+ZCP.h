//
//  UIButton+ZCP.h
//  ZCPCategory
//
//  Created by zcp on 2019/5/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (ZCP)

- (void)addAction:(void (^)(UIButton *button))block;
- (void)addActionNoMultiTouchFilter:(void (^)(UIButton *button))block;
- (void)addAction:(void (^)(UIButton *button))block forControlEvents:(UIControlEvents)controlEvents;
- (void)addActionNoMultiTouchFilter:(void (^)(UIButton *button))block forControlEvents:(UIControlEvents)controlEvents;

- (void)updateSubviewFrameWithTitleSize:(CGSize)titleSize imageSize:(CGSize)imageSize position:(NSInteger)position gap:(CGFloat)gap;

@end

NS_ASSUME_NONNULL_END
