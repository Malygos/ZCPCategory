//
//  UIFont+ZCPAdd.m
//  ZCPCategory
//
//  Created by zcp on 2019/6/17.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "UIFont+ZCPAdd.h"

@implementation UIFont (ZCPAdd)

+ (UIFont *)zcp_pingFangSCFontOfSize:(CGFloat)fontSize weight:(ZCPFontWeight)fontWeight {
    if ([UIDevice currentDevice].systemVersion.doubleValue >= 9) {
        switch (fontWeight) {
            case ZCPFontWeightUltraLight:
                return [UIFont fontWithName:@"PingFangSC-Ultralight" size:fontSize];
                break;
            case ZCPFontWeightThin:
                return [UIFont fontWithName:@"PingFangSC-Thin" size:fontSize];
                break;
            case ZCPFontWeightLight:
                return [UIFont fontWithName:@"PingFangSC-Light" size:fontSize];
                break;
            case ZCPFontWeightRegular:
                return [UIFont fontWithName:@"PingFangSC-Regular" size:fontSize];
                break;
            case ZCPFontWeightMedium:
                return [UIFont fontWithName:@"PingFangSC-Medium" size:fontSize];
                break;
            case ZCPFontWeightSemibold:
                return [UIFont fontWithName:@"PingFangSC-Semibold" size:fontSize];
                break;
            default:
                return nil;
                break;
        }
    } else if (@available(iOS 8.2, *)) {
        switch (fontWeight) {
            case ZCPFontWeightUltraLight:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightUltraLight];
                break;
            case ZCPFontWeightThin:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightThin];
                break;
            case ZCPFontWeightLight:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightLight];
                break;
            case ZCPFontWeightRegular:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightRegular];
                break;
            case ZCPFontWeightMedium:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightMedium];
                break;
            case ZCPFontWeightSemibold:
                return [UIFont systemFontOfSize:fontSize weight:UIFontWeightSemibold];
                break;
            default:
                return nil;
                break;
        }
    } else {
        return [UIFont systemFontOfSize:fontSize];
    }
}

+ (UIFont *)zcp_systemFontOfSize:(CGFloat)fontSize {
    return [UIFont zcp_pingFangSCFontOfSize:fontSize weight:ZCPFontWeightRegular];
}

+ (UIFont *)zcp_boldSystemFontOfSize:(CGFloat)fontSize {
    return [UIFont zcp_pingFangSCFontOfSize:fontSize weight:ZCPFontWeightSemibold];
}

+ (UIFont *)zcp_mediumSystemFontSize:(CGFloat)fontSize {
    return [UIFont zcp_pingFangSCFontOfSize:fontSize weight:ZCPFontWeightMedium];
}

@end
