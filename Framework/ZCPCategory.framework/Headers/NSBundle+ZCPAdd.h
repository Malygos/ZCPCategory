//
//  NSBundle+ZCPAdd.h
//  ZCPCategory
//
//  Created by zcp on 2019/5/22.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSBundle (ZCPAdd)

// 应用名称
+ (NSString *)appDisplayName;
/// 应用包名
+ (NSString *)appBundleIdentifier;
/// 应用版本号
+ (NSString *)appVersion;
/// 编译版本号
+ (NSString *)appBuildVersion;

@end

NS_ASSUME_NONNULL_END
