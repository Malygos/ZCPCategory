//
//  UIFont+ZCPAdd.h
//  ZCPCategory
//
//  Created by zcp on 2019/6/17.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 由于UIFontWeight iOS 8.2以上可用，所以这里自定义了枚举类型
typedef NS_ENUM(NSInteger, ZCPFontWeight) {
    ZCPFontWeightUltraLight,
    ZCPFontWeightThin,
    ZCPFontWeightLight,
    ZCPFontWeightRegular,
    ZCPFontWeightMedium,
    ZCPFontWeightSemibold,
    ZCPFontWeightBold,
    ZCPFontWeightHeavy,
    ZCPFontWeightBlack
};

@interface UIFont (ZCPAdd)

#pragma mark - ping fang sc 字体

+ (UIFont *)zcp_pingFangSCFontOfSize:(CGFloat)fontSize weight:(ZCPFontWeight)fontWeight;

+ (UIFont *)zcp_systemFontOfSize:(CGFloat)fontSize;

+ (UIFont *)zcp_boldSystemFontOfSize:(CGFloat)fontSize;

+ (UIFont *)zcp_mediumSystemFontSize:(CGFloat)fontSize;

@end

NS_ASSUME_NONNULL_END
