//
//  UIDevice+ZCPAdd.h
//  ZCPCategory
//
//  Created by zcp on 2019/5/22.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 运营商
typedef NS_ENUM(NSInteger, ZCPChinaCarrierType) {
    ZCPChinaCarrierTypeUnknown,         //!< 未知
    ZCPChinaCarrierTypeChinaMobile,     //!< 中国移动
    ZCPChinaCarrierTypeChinaUnicom,     //!< 中国联通
    ZCPChinaCarrierTypeChinaTelecom,    //!< 中国电信
    ZCPChinaCarrierTypeChinaTietong     //!< 中国铁通
};

/// 权限可用状态
typedef NS_ENUM(NSInteger, ZCPPermissionAvailableStatus) {
    ZCPPermissionAvailableStatusNotDetermined   = -1,   //!< 用户还未选择
    ZCPPermissionAvailableStatusNotAvailable    = 0,    //!< 用户拒绝或不可用
    ZCPPermissionAvailableStatusAvailable       = 1     //!< 可用
};

@interface UIDevice (ZCPAdd)

// ----------------------------------------------------------------------
#pragma mark - Size
// ----------------------------------------------------------------------

/// 状态栏高度
+ (CGFloat)stautsBarHeight;
/// 状态栏+导航栏高度
+ (CGFloat)stautsBarAndNaviBarHeight;
/// 标签栏高度
+ (CGFloat)tabBarHeight;
/// 安全区底部外边距
+ (CGFloat)safeAreaInsetsBottomHeight;

// ----------------------------------------------------------------------
#pragma mark - Device Information
// ----------------------------------------------------------------------

/// 设备名称，如：zcp的iphone
+ (NSString *)deviceName;
/// 系统名称，如：iOS
+ (NSString *)OSName;
/// 系统版本，如：12.3
+ (NSString *)OSVersion;
/// 运营商
+ (ZCPChinaCarrierType)carrier;
/// 国家代码，如：CN
+ (NSString *)countryCode;
/// 语言，如：zh-Hans-CN
+ (NSString *)language;
/// 时区，如：Asia/Shanghai
+ (NSString *)timeZone;
/// 分辨率，如：414x736
+ (NSString *)resolution;
/// 设备型号，如：iPhone11,6
+ (NSString *)deviceModel;
/// 设备型号名称，如：iPhone XS MAX
+ (NSString *)deviceModelName;

/// 是否是iPhone
+ (BOOL)isIPhone;
/// 是否是iPhone Plus
+ (BOOL)isIphonePlus;
/// 是否是iPad
+ (BOOL)isIpad;
/// 是否是iPhoneX系列
+ (BOOL)isIPhoneXSeries;

// ----------------------------------------------------------------------
#pragma mark - Identifier
// ----------------------------------------------------------------------

/// UDID，iOS不可用，2013年5月1日开始，采集UDID的App会被拒绝上架App Store
+ (NSString *)UDID NS_UNAVAILABLE;
/// 设备mac地址，iOS 7以后不可用，获取到的值固定为02:00:00:00:00:00
+ (NSString *)macAddress NS_UNAVAILABLE;
/// 广告标示符
+ (NSString *)IDFA;
/// 应用提供商标示符
+ (NSString *)IDFV;

// ----------------------------------------------------------------------
#pragma mark - Security
// ----------------------------------------------------------------------

/// 判断App是否被破解
+ (BOOL)isPirated;
/// 判断设备是否越狱
+ (BOOL)isJailbroken;


// ----------------------------------------------------------------------
#pragma mark - Permission Available
// ----------------------------------------------------------------------

+ (ZCPPermissionAvailableStatus)isLocationAvailable;
+ (ZCPPermissionAvailableStatus)isCameraAvailable;
+ (ZCPPermissionAvailableStatus)isAudioAvailable;
+ (ZCPPermissionAvailableStatus)isPhotoAvailable;
+ (ZCPPermissionAvailableStatus)isAddressAvailable;
+ (ZCPPermissionAvailableStatus)isCalendarAvailable;
+ (ZCPPermissionAvailableStatus)isRemindAvailable;
+ (BOOL)isPushAvailable;
+ (BOOL)isQRCodeScanAvailable;

@end

NS_ASSUME_NONNULL_END
